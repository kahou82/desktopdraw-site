from django.views.generic.base import View
from django.http import HttpResponse
from django.shortcuts import render, redirect
from showcase.models import Comment

class HomeView(View):
	def get(self, request):
		return render(request, 'home.html')


class FeaturesView(View):
	def get(self, request):
		return render(request, 'feature.html')


class ContactView(View):
	def get(self, request):
		return render(request, 'contact.html')

	def post(self, request):
		comment = request.POST["comment_input"]

		if len(comment) >= 4:
			comment_obj = Comment()
			comment_obj.comment = comment
			comment_obj.save()

		return redirect('/features')